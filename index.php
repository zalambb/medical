<?php



// Report all PHP errors (see changelog)

ini_set("display_errors", "0");

error_reporting(E_ALL & ~E_WARNING);



?>



<!DOCTYPE html>

<html lang="en">

<!--<![endif]-->

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta charset="utf-8">

<title>Coming Soon Page - Website Design & Development Services,Mobile Application, Module development services, VoIP Service Provider India | XpertBrain Technology India</title>

<meta http-equiv="X-UA-Compatible" content="chrome=1">

<meta name="description" content="XpertBrain Tehnology : A web site design & development company India which also offers custom graphics designs, Website redesigns, website hosting and building ,Mobile Apps, Voip Services and  effective Internet Marketing services SEO, SMO, SEM from India">

<meta name="keywords" content="XpertBrain Tehnology, Web Design Company, Web Development, Custom Web Design Development, VoIP Business solutions company India, VoIP customization services,Mobile Application Services, voip services provider company India"/>

<meta name="meta_business" content="XpertBrain Technology is a Web Design & Development, VoIP software, mobile application developemnt Company. It offers creative graphic designing and effective Internet Marketing services SEO, SMO, SEM. " />

<meta name="meta_copyright" content="www.xpertbrain.com " />

<meta name="meta_identify_url" content="http://www.xpertbrain.com " />

<meta name="meta_author" content="Web Design & Development, SEO - SEM - SMO Professionals, VoIP Service Providers, XpertBrain Technology " />

<meta name="robots" content="index, follow " />

<meta name="meta_revisit_after" content="7 days " />

<meta name="language" content="en-gb " />

<meta name="meta_rating" content="General " />

<meta name="meta_audience" content="All " />

<meta name="meta_coverage" content="Worldwide " />

<meta name="meta_publisher" content="www.xpertbrain.com " />

<meta name="meta_document_state" content="Static " />

<meta http-equiv="YahooSeeker" content="index, follow" />

<meta http-equiv="msnbot" content="index, follow" />

<meta http-equiv="googlebot" content="index, follow" />

<meta http-equiv="allow-search" content="yes" />



<link rel="stylesheet" type="text/css" href="resources/css">

<link rel="stylesheet" type="text/css" href="resources/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="resources/styles.css">

</head>



<body id="home" cz-shortcut-listen="true">

<section class="main">

  <div id="Content" class="wrapper topSection">

    <div id="Header">

      <div class="wrapper">

        <div class="logo">

          <h1>XPERTBRAIN TECHNOLOGY</h1>

        </div>

      </div>

    </div>

    <h2>We are coming soon!!!</h2>

    <div class="countdown styled">

      <div class="dys">143 <span>days</span></div>

      <div class="hrs">14 <span>hrs</span></div>

      <div class="mns">42 <span>min</span></div>

      <div class="scs">29 <span>sec</span></div>

    </div>

  </div>	

</section>

<section class="subscribe spacing">

  <div class="container">

    <div id="subscribe">

     <!-- <h3>Subscribe To Get Notified</h3>

      <form action="subscribe.php" method="post" onsubmit="">

        <p>

          <input name="emailid" value="Enter your e-mail" type="text" id="emailid" />

          <input type="button" value="Submit">

        </p>

      </form>-->

      

      <h3>Reach out to XPERTBRAIN </h3>

      

      <div id="socialIcons">

        <ul>

          <li><a target="_blank" href="https://twitter.com/XpertBrainTech" title="Twitter" class="twitterIcon"></a></li>

          <li class="fb_icn"><a target="_blank" href="https://www.facebook.com/XpertBrain" title="facebook" class="facebookIcon"></a></li>

          <li><a target="_blank" href="https://www.pinterest.com/xpertbraintech/" title="Pintrest" class="pintrestIcon"></a></li>

          <li class="pi_icn"><a target="_blank" href="https://plus.google.com/u/1/109424910084739425158" title="googlePlus" class="gplusIcon"></a></li>

        </ul>

      </div>

    </div>

  </div>

</section>

<section class="features spacing">

  <div class="container">

    <h2 class="text-center">Services</h2>

    <div class="row">

      <div class="col-md-6">

        <div class="featuresPro">

          <div class="col-md-3 col-sm-3 col-xs-3 text-center"><img src="resources/icon-1.png" data-at2x="img/icon-1@2x.png" alt="Features"></div>

          <div class="col-md-9 col-sm-9 col-xs-9"> 

            <!--features 1-->

            <h4>Web Design and Development</h4>

            <p>Every web design is designed and developed by us. This means we can ensure the highest quality websites every time.</p>

            <!--features 1 end--> 

          </div>

        </div>

        <div class="featuresPro">

          <div class="col-md-3 col-sm-3 col-xs-3 text-center"><img src="resources/icon-2.png" data-at2x="img/icon-2@2x.png" alt="Features"></div>

          <div class="col-md-9 col-sm-9 col-xs-9"> 

            <!--features 2-->

            <h4>Internet Services</h4>

            <p>We are Leading Internet Marketing Services and SEO Company in Ahmedabad India, offering wider variety of Services like SEO,SMO, SME,Google Adwords, PPC etc.</p>

            <!--features 2 end--> 

          </div>

        </div>

      </div>

      <div class="col-md-6">

        <div class="featuresPro">

          <div class="col-md-3 col-sm-3 col-xs-3 text-center"><img src="resources/icon-3.png" data-at2x="img/icon-3@2x.png" alt="Features"></div>

          <div class="col-md-9 col-sm-9 col-xs-9"> 

            <!--features 3-->

            <h4>Mobile Applications</h4>

            <p>Xpert Brain Technology is one of the leading mobile application development company from India that started making Android, Windows and iOS apps.</p>

            <!--features 3 end--> 

          </div>

        </div>

        <div class="featuresPro">

          <div class="col-md-3 col-sm-3 col-xs-3 text-center"><img src="resources/icon-4.png" data-at2x="img/icon-4@2x.png" alt="Features"></div>

          <div class="col-md-9 col-sm-9 col-xs-9"> 

            <!--features 4-->

            <h4>VOIP</h4>

            <p>Xpert Brain Technology is one of the few company which provides VoIP services, module development and customization services by skilled VoIP programmers at very effective cost.</p>

            <!--features 4 end--> 

          </div>

        </div>

      </div>

    </div>

  </div>

</section>

<span class="tempBy"> Copyright &copy; <?php echo date('Y') ?> <a href="http://www.xpertbrain.com/" alt="xpertbrain">xpertbrain.com</a> All rights reserved </span> 



<!--Scripts--> 

<script type="text/javascript" src="resources/jquery-1.9.1.min.js"></script> 

<script type="text/javascript" src="resources/jquery.countdown.js"></script> 

<script type="text/javascript" src="resources/global.js"></script>

</body>

</html>